# TicketBuster Design Document

# Frontend Specifications
Frontend rendering is handled by react-rails, data is requested from the Rails API, and returned in JSON. 

There will be only four views needed:
_login will be the login page
_admin will be rails-admin
_tickets will provide a feed of all tickets assigned to user
_report will return a report of closed tickets for the month

# API Design
All endpoints are JSON only.

## Tickets Structure
### `/api/v1/tickets`

`POST /api/v1/tickets` -- Takes a JSON array of parameters and creates a new ticket. 
summary:string
user_token:string

```
$ curl -i -X POST localhost:3000/api/v1/tickets -d params={"summary":"This text summarizes the issue at hand", "priority": "0", "status": "0", "message": "This is a long string explaining in detail."}
HTTP/1.1 201 Created
...
```

`GET /api/v1/tickets` -- gets all tickets assigned to or owned by logged in user. 

`GET /api/v1/ticket/:ticket_id` -- get specific ticket data.

`GET /api/v1/tickets/open` -- gets all unassigned tickets.

`DELETE /api/v1/ticket/1234` -- removes ticket 1234. Must be logged in user.

## Users Structure

`/api/v1/users`

`POST /api/v1/users/login` -- authenticate

`GET /api/v1/users` -- see all users. Admin only.

Users will be authenticated with Devise, and have three levels of permissions:

 1. Customers -- lowest tier
 2. Agents -- second tier
 3. Admins -- highest tier

All Users should be able to authenticate normally, i.e., with username/email and password combination, or with Google OAuth, Facebook, or Twitter login. 

All Users can log in via `/api/v1/login`
Admin users will be redirected to `/admin` and can CRUD any system object. 
Agent users will be redirected to `/tickets` and can CRUD any ticket assigned to them.
Customer users will be redirected to `/tickets`

# Database Design
Data store is mysql. Index and	`NOT NULL` applied where appropriate.