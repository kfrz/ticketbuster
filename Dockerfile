FROM ruby:2.3.1-slim
MAINTAINER Keifer Furzland <kfrz.code@gmail.com>

# Env
ENV REFRESHED_AT 2016-09-23
ENV REPO_DIR /home/ticketbuster/repo
ENV GEM_HOME /home/ticketbuster/gems
ENV ENV_FILE /home/ticketbuster/repo/.env

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -yqq libyaml-dev \
    libcurl4-openssl-dev libxslt-dev build-essential nodejs libmysqlclient-dev libev-dev

# Create and switch to repo dir
RUN mkdir -p $GEM_HOME $REPO_DIR
WORKDIR $REPO_DIR

# Copy the rest of the app
# RUN git clone ssh://git@gitlab.com:kfrz/ticketbuster.git
ADD . $REPO_DIR

# Symlink vendor
RUN ln -sn /home/ticketbuster/repo/vendor/bundle $GEM_HOME

# Install gems. Bundler won't allow us to install with documentation.
#### NOTE: In production, the bundle command should be appended with `--without development test`
COPY Gemfile* $REPO_DIR/
RUN bundle install --jobs=6 --path=$GEM_HOME

# Provide a Healthcheck for Docker risk mitigation
HEALTHCHECK --interval=3600s --timeout=20s --retries=2 CMD curl http://localhost:3000 || exit 1

# Define an entrypoint for default
ENTRYPOINT ["bundle", "exec"]

# Default command, running app as a service 
CMD ["bundle", "exec", "foreman", "start"] 
