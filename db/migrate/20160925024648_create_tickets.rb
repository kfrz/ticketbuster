class CreateTickets < ActiveRecord::Migration[5.0]
  def change
    create_table :tickets do |t|
      t.string :summary, null: false
      t.text :content
      t.integer :status, null:false
      t.integer :priority, default: 0

      t.timestamps
    end
  end
end
